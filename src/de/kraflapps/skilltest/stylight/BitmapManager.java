package de.kraflapps.skilltest.stylight;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;

/**
 * 
 * @author Veronika Rodionova
 * 
 */

public enum BitmapManager {

	INSTANCE;

	private static final int ON_SAMPLE_SIZE = 4;
	private static final int THREAD_POOL = 5;
	private static final String TAG = "BitmapManager";

	private final Map<String, SoftReference<Bitmap>> cache;
	private final ExecutorService pool;
	private Map<ImageView, String> imageViews = Collections
			.synchronizedMap(new WeakHashMap<ImageView, String>());
	private Bitmap placeholder;

	BitmapManager() {
		cache = new HashMap<String, SoftReference<Bitmap>>();
		pool = Executors.newFixedThreadPool(THREAD_POOL);
	}

	public void setPlaceholder(final Bitmap bitmap) {
		placeholder = bitmap;
	}

	public Bitmap getBitmapFromCache(final String imageURL) {
		Bitmap bitmap = null;
		if (cache.containsKey(imageURL)) {
			bitmap = cache.get(imageURL).get();
		}
		return bitmap;
	}

	public void queueJob(final String imageURL, final ImageView imageView,
			final int width, final int height, final boolean isOnSale) {

		final Handler handler = new Handler(new IncomingHandlerCallback(
				imageURL, imageView));

		pool.submit(new Runnable() {
			@Override
			public void run() {
				final Bitmap bitmap = loadBitmapToCache(imageURL, width,
						height, isOnSale);
				final Message message = Message.obtain();
				message.obj = bitmap;
				Log.d(TAG, "Item downloaded: " + imageURL);
				handler.sendMessage(message);
			}
		});
	}

	public void loadBitmapToView(final String imageURL,
			final ImageView imageView, final int width, final int height,
			final boolean isOnSale) {
		imageViews.put(imageView, imageURL);
		Bitmap bitmap = getBitmapFromCache(imageURL);

		if (bitmap != null) {
			Log.d(TAG, "Item loaded from cache: " + imageURL);
			if (!isOnSale) {
				bitmap = toGrayscale(bitmap);
			}
			imageView.setImageBitmap(bitmap);
		} else {
			imageView.setImageBitmap(placeholder);
			queueJob(imageURL, imageView, width, height, isOnSale);
		}
	}

	private Bitmap loadBitmapToCache(final String imageURL, final int width,
			final int height, final boolean isOnSale) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		Bitmap bitmap = null;

		try {
			final InputStream inputStream = new java.net.URL(imageURL)
					.openStream();
			options.inSampleSize = ON_SAMPLE_SIZE;
			options.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeStream(inputStream, null, options);
			if (!isOnSale) {
				bitmap = toGrayscale(bitmap);
			}
			cache.put(imageURL, new SoftReference<Bitmap>(bitmap));
		} catch (MalformedURLException e) {
			Log.d(LoginActivity.TAG, e.getMessage(), e);
		} catch (IOException e) {
			Log.d(LoginActivity.TAG, e.getMessage(), e);
		}

		return bitmap;
	}

	public Bitmap toGrayscale(final Bitmap originalBitmap) {
		int width;
		int height;
		height = originalBitmap.getHeight();
		width = originalBitmap.getWidth();

		final Bitmap bmpGrayscale = Bitmap.createBitmap(width, height,
				Bitmap.Config.RGB_565);
		final Canvas canvas = new Canvas(bmpGrayscale);
		final Paint paint = new Paint();
		final ColorMatrix colorMatrix = new ColorMatrix();
		colorMatrix.setSaturation(0);
		final ColorMatrixColorFilter filter = new ColorMatrixColorFilter(
				colorMatrix);
		paint.setColorFilter(filter);
		canvas.drawBitmap(originalBitmap, 0, 0, paint);
		return bmpGrayscale;
	}

	public class IncomingHandlerCallback implements Handler.Callback {

		private String imageURL;
		private ImageView imageView;

		public IncomingHandlerCallback(final String imageURL,
				final ImageView imageView) {
			this.imageURL = imageURL;
			this.imageView = imageView;
		}

		@Override
		public boolean handleMessage(final Message message) {

			final String tag = imageViews.get(imageView);
			if (tag != null && tag.equals(imageURL)) {
				if (message.obj != null) {
					imageView.setImageBitmap((Bitmap) message.obj);
				} else {
					imageView.setImageBitmap(placeholder);
					Log.d(TAG, "failed to load: " + imageURL);
				}
			}
			return true;
		}
	}
}
