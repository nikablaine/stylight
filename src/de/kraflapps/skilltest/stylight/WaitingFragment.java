package de.kraflapps.skilltest.stylight;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * 
 * @author Veronika Rodionova
 * 
 */

public class WaitingFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			final Bundle savedInstanceState) {

		final View rootView = inflater.inflate(R.layout.fragment_wait,
				container, false);

		return rootView;
	}
}
