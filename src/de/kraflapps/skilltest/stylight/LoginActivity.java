package de.kraflapps.skilltest.stylight;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * 
 * @author Veronika Rodionova
 * 
 */

public class LoginActivity extends FragmentActivity {

	public static final String TAG = "Stylight-App";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		if (savedInstanceState == null) {
			final SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(this);
			final String cookie = preferences.getString(
					LoginTask.COOKIE_ST_SECURE, null);

			if (cookie != null) {
				final GetInfoTask getInfoTask = new GetInfoTask(this);
				getInfoTask.execute();
				getSupportFragmentManager().beginTransaction()
						.add(R.id.container, new WaitingFragment()).commit();
			} else {
				getSupportFragmentManager().beginTransaction()
						.add(R.id.container, new LoginFragment()).commit();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		final Context context = getApplicationContext();
		final int id = item.getItemId();
		if (id == R.id.action_log_out) {
			final SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(context);
			final String cookie = preferences.getString(
					LoginTask.COOKIE_ST_SECURE, null);
			if (cookie == null) {
				Toast.makeText(context,
						getString(R.string.string_not_logged_in),
						Toast.LENGTH_SHORT).show();
			} else {
				final SharedPreferences.Editor editor = preferences.edit();
				editor.remove(LoginTask.COOKIE_ST_SECURE);
				editor.commit();
				getSupportFragmentManager().beginTransaction()
						.add(R.id.container, new LoginFragment()).commit();
			}
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater,
				final ViewGroup container, final Bundle savedInstanceState) {
			final View rootView = inflater.inflate(R.layout.fragment_login,
					container, false);
			return rootView;
		}
	}

}
