package de.kraflapps.skilltest.stylight;

import java.io.Serializable;

/**
 * 
 * @author Veronika Rodionova
 * 
 */

public class ItemInfo implements Serializable {

	private static final long serialVersionUID = 8216312689664314815L;

	private final String description;
	private final double price;
	private final double priceOld;
	private final String imageLink;
	private final boolean onSale;

	public ItemInfo(final String description, final double price,
			final double priceOld, final String imageLink,
			final boolean isOnSale) {
		this.description = description;
		this.price = price;
		this.priceOld = priceOld;
		this.imageLink = imageLink;
		this.onSale = isOnSale;
	}

	public String getDescription() {
		return description;
	}

	public double getPrice() {
		return price;
	}

	public double getPriceOld() {
		return priceOld;
	}

	public String getImageLink() {
		return imageLink;
	}

	public boolean isOnSale() {
		return onSale;
	}

}
