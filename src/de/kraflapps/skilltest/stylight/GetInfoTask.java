package de.kraflapps.skilltest.stylight;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

/**
 * 
 * @author Veronika Rodionova
 * 
 */

public class GetInfoTask extends AsyncTask<Void, Void, JSONObject> {

	public static final String STYLIGHT_GET_URL = "http://api.stylight.de/api/products?request_locale=de_DE&sale=true&sortBy=popularity&pageItems=89";
	public static final String KEYNAME_ACCEPT_LANGUAGE = "Accept-Language";
	public static final String KEYNAME_COOKIE = "Cookie";
	public static final String KEYNAME_API_KEY = "X-apiKey";
	public static final String KEYNAME_PRODUCT_LIST = "productlist";
	public static final String ACCEPT_LANGUAGE = "de-De";

	public final FragmentActivity activity;
	private String textInToast;

	public GetInfoTask(FragmentActivity fragmentActivity) {
		super();
		activity = fragmentActivity;
	}

	@Override
	protected JSONObject doInBackground(Void... params) {

		final SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(activity);
		final String cookie = prefs.getString(LoginTask.COOKIE_ST_SECURE, null);

		final HttpClient httpClient = new DefaultHttpClient();

		final HttpGet httpGet = new HttpGet(STYLIGHT_GET_URL);
		HttpResponse httpResponse = null;
		JSONObject parseResponse = null;

		httpGet.addHeader(KEYNAME_ACCEPT_LANGUAGE, ACCEPT_LANGUAGE);
		httpGet.addHeader(KEYNAME_COOKIE, KEYNAME_COOKIE + '=' + cookie);
		httpGet.addHeader(KEYNAME_API_KEY, LoginFragment.X_API_KEY);

		try {
			httpResponse = httpClient.execute(httpGet);
		} catch (ClientProtocolException e) {
			setTextInToast(activity.getResources().getString(
					R.string.string_http_error));
			Log.e(LoginActivity.TAG, getTextInToast(), e);
		} catch (IOException e) {
			setTextInToast(activity.getResources().getString(
					R.string.string_io_error));
			Log.e(LoginActivity.TAG, getTextInToast(), e);
		}

		if (httpResponse != null) {
			final StatusLine statusLine = httpResponse.getStatusLine();
			final int statusCode = statusLine.getStatusCode();

			if (statusCode == LoginTask.SUCCESS_STATUS_CODE) {
				final HttpEntity entity = httpResponse.getEntity();
				try {
					parseResponse = parseResponse(entity);
				} catch (JSONException e) {
					setTextInToast(activity.getResources().getString(
							R.string.string_parsing_error));
				} catch (UnsupportedEncodingException e) {
					setTextInToast(activity.getResources().getString(
							R.string.string_parsing_error_enc));
					Log.e(LoginActivity.TAG, getTextInToast(), e);
				} catch (IllegalStateException e) {
					setTextInToast(activity.getResources().getString(
							R.string.string_parsing_error));
					Log.e(LoginActivity.TAG, getTextInToast(), e);
				} catch (IOException e) {
					setTextInToast(activity.getResources().getString(
							R.string.string_io_error));
					Log.e(LoginActivity.TAG, getTextInToast(), e);
				}

			}
		}
		return parseResponse;
	}

	protected void onPostExecute(JSONObject result) {

		if (getTextInToast() != null) {
			// there were exceptions, showing information to user
			Toast.makeText(activity, getTextInToast(), Toast.LENGTH_SHORT)
					.show();
		} else {
			JSONArray productList = null;
			try {
				productList = result.getJSONArray(KEYNAME_PRODUCT_LIST);
			} catch (JSONException e) {
				final String msg = activity.getResources().getString(
						R.string.string_parsing_error);
				Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
				Log.e(LoginActivity.TAG, msg, e);
			}

			if (productList != null) {
				final Bundle data = new Bundle();
				data.putString(InfoFragment.KEYNAME_INFO,
						productList.toString());
				final InfoFragment infoFragment = new InfoFragment();
				infoFragment.setArguments(data);
				activity.getSupportFragmentManager().beginTransaction()
						.replace(R.id.container, infoFragment).commit();
			}
		}

	}

	private JSONObject parseResponse(HttpEntity entity) throws JSONException,
			UnsupportedEncodingException, IllegalStateException, IOException {
		JSONObject finalResult = null;

		final BufferedReader reader = new BufferedReader(new InputStreamReader(
				entity.getContent(), "UTF-8"));
		final StringBuilder builder = new StringBuilder();
		for (String line = null; (line = reader.readLine()) != null;) {
			builder.append(line).append("\n");
		}
		final JSONTokener tokener = new JSONTokener(builder.toString());
		finalResult = new JSONObject(tokener);
		return finalResult;
	}

	public String getTextInToast() {
		return textInToast;
	}

	public void setTextInToast(String textInToast) {
		this.textInToast = textInToast;
	}

}
