package de.kraflapps.skilltest.stylight;

import java.io.Serializable;

/**
 * 
 * @author Veronika Rodionova
 * 
 */

public class PairInfo implements Serializable {

	private static final long serialVersionUID = -3024352747725109445L;

	private final String descriptionLeft;
	private final double priceLeft;
	private final double priceOldLeft;
	private final String imageLinkLeft;
	private final boolean onSaleLeft;
	private final String descriptionRight;
	private final double priceRight;
	private final double priceOldRight;
	private final String imageLinkRight;
	private final boolean onSaleRight;

	public PairInfo(final String descriptionLeft, final double priceLeft,
			final double priceOldLeft, final String imageLinkLeft,
			final boolean isOnSaleLeft, final String descriptionRight,
			final double priceRight, final double priceOldRight,
			final String imageRight, final boolean isOnSaleRight) {
		this.descriptionLeft = descriptionLeft;
		this.priceLeft = priceLeft;
		this.priceOldLeft = priceOldLeft;
		this.imageLinkLeft = imageLinkLeft;
		this.onSaleLeft = isOnSaleLeft;
		this.descriptionRight = descriptionRight;
		this.priceRight = priceRight;
		this.priceOldRight = priceOldRight;
		this.imageLinkRight = imageRight;
		this.onSaleRight = isOnSaleRight;
	}

	public PairInfo(final ItemInfo leftItemInfo, final ItemInfo rightItemInfo) {
		this.descriptionLeft = leftItemInfo.getDescription();
		this.priceLeft = leftItemInfo.getPrice();
		this.priceOldLeft = leftItemInfo.getPriceOld();
		this.imageLinkLeft = leftItemInfo.getImageLink();
		this.onSaleLeft = leftItemInfo.isOnSale();
		this.descriptionRight = rightItemInfo.getDescription();
		this.priceRight = rightItemInfo.getPrice();
		this.priceOldRight = rightItemInfo.getPriceOld();
		this.imageLinkRight = rightItemInfo.getImageLink();
		this.onSaleRight = rightItemInfo.isOnSale();
	}

	public String getDescriptionLeft() {
		return descriptionLeft;
	}

	public double getPriceLeft() {
		return priceLeft;
	}

	public String getImageLinkLeft() {
		return imageLinkLeft;
	}

	public String getDescriptionRight() {
		return descriptionRight;
	}

	public double getPriceRight() {
		return priceRight;
	}

	public String getImageLinkRight() {
		return imageLinkRight;
	}

	public boolean isOnSaleLeft() {
		return onSaleLeft;
	}

	public boolean isOnSaleRight() {
		return onSaleRight;
	}

	public double getPriceOldLeft() {
		return priceOldLeft;
	}

	public double getPriceOldRight() {
		return priceOldRight;
	}

}
