package de.kraflapps.skilltest.stylight;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author Veronika Rodionova
 * 
 */

public class PairInfoAdapter extends BaseAdapter {

	public static final int IMAGE_WIDTH = 200;
	public static final int IMAGE_HEIGHT = 200;

	private final FragmentActivity activity;
	private final List<PairInfo> productList;
	private LayoutInflater inflater;
	private final String euroSymbol;

	public PairInfoAdapter(final FragmentActivity fragmentActivity,
			final List<PairInfo> productList) {
		super();
		this.activity = fragmentActivity;
		this.productList = productList;
		inflater = (LayoutInflater) fragmentActivity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		euroSymbol = activity.getResources().getString(
				R.string.string_currency_euro);
	}

	@Override
	public int getCount() {
		return productList.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View returnView = convertView;
		ViewHolder holder;
		if (returnView == null) {
			returnView = inflater.inflate(R.layout.list_row, null);
			holder = new ViewHolder();
			holder.itemImageLeft = (ImageView) returnView
					.findViewById(R.id.list_image_left);
			holder.itemDescriptionLeft = (TextView) returnView
					.findViewById(R.id.description_left);
			holder.itemPriceLeft = (TextView) returnView
					.findViewById(R.id.price_left);
			holder.itemPriceOldLeft = (TextView) returnView
					.findViewById(R.id.price_left_old);
			holder.itemImageRight = (ImageView) returnView
					.findViewById(R.id.list_image_right);
			holder.itemDescriptionRight = (TextView) returnView
					.findViewById(R.id.description_right);
			holder.itemPriceRight = (TextView) returnView
					.findViewById(R.id.price_right);
			holder.itemPriceOldRight = (TextView) returnView
					.findViewById(R.id.price_right_old);
			returnView.setTag(holder);
		} else {
			holder = (ViewHolder) returnView.getTag();
		}

		holder.position = position;

		final PairInfo item = productList.get(position);

		drawInterface(holder.itemImageLeft, holder.itemDescriptionLeft,
				holder.itemPriceLeft, holder.itemPriceOldLeft,
				item.getImageLinkLeft(), item.getDescriptionLeft(),
				Double.compare(item.getPriceLeft(), Double.NaN) == 0 ? null
						: String.format("%.2f", item.getPriceLeft())
								+ euroSymbol + " ",
				String.format("%.2f", item.getPriceOldLeft()) + euroSymbol,
				item.isOnSaleLeft());

		drawInterface(holder.itemImageRight, holder.itemDescriptionRight,
				holder.itemPriceRight, holder.itemPriceOldRight,
				item.getImageLinkRight(), item.getDescriptionRight(),
				Double.compare(item.getPriceRight(), Double.NaN) == 0 ? null
						: String.format("%.2f", item.getPriceRight())
								+ euroSymbol + " ",
				String.format("%.2f", item.getPriceOldRight()) + euroSymbol,
				item.isOnSaleRight());

		return returnView;
	}

	public static class ViewHolder {

		public ImageView itemImageLeft;
		public TextView itemDescriptionLeft;
		public TextView itemPriceLeft;
		public TextView itemPriceOldLeft;
		public ImageView itemImageRight;
		public TextView itemDescriptionRight;
		public TextView itemPriceRight;
		public TextView itemPriceOldRight;
		public int position;
	}

	private void drawInterface(final ImageView itemImage,
			final TextView itemDescription, final TextView itemPrice,
			final TextView itemPriceOld, final String imageLink,
			final String description, final String price,
			final String priceOld, final boolean isOnSale) {

		BitmapManager.INSTANCE.loadBitmapToView(imageLink, itemImage,
				IMAGE_WIDTH, IMAGE_HEIGHT, isOnSale);
		itemDescription.setText(description);
		itemPrice.setText(price);
		if (isOnSale) {
			itemPrice.setTextColor(Color.RED);
			itemPriceOld.setPaintFlags(itemPriceOld.getPaintFlags()
					| Paint.STRIKE_THRU_TEXT_FLAG);
			itemPriceOld.setText(priceOld);
		} else {
			itemPrice.setTextColor(Color.GRAY);
			itemPriceOld.setText(null);
		}
	}

}
