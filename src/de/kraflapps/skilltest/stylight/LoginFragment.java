package de.kraflapps.skilltest.stylight;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * 
 * @author Veronika Rodionova
 * 
 */

public class LoginFragment extends Fragment {

	public static final String STYLIGHT_URL = "http://api.stylight.com/api/login";
	public static final String X_API_KEY = "D13A5A5A0A3602477A513E02691A8458";
	public static final String KEYNAME_LOGIN = "username";
	public static final String KEYNAME_PASSWORD = "passwd";
	public static final String KEYNAME_API_KEY = "apiKey";
	public static final String KEYNAME_COOKIE = "st_secure";

	public EditText etLogin;
	public EditText etPassword;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View rootView = inflater.inflate(R.layout.fragment_login,
				container, false);

		etLogin = (EditText) rootView.findViewById(R.id.editTextLogin);
		etPassword = (EditText) rootView.findViewById(R.id.editTextPassword);

		final Button btnLogin = (Button) rootView
				.findViewById(R.id.buttonLogin);
		btnLogin.setOnClickListener(btnLoginClickListener);

		return rootView;

	}

	private View.OnClickListener btnLoginClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View view) {

			final FragmentActivity activity = getActivity();

			final String textLogin = etLogin.getText().toString();
			final String textPassword = etPassword.getText().toString();

			final InputMethodManager inputManager = (InputMethodManager) activity
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow(activity.getCurrentFocus()
					.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

			if (textLogin.isEmpty() || textPassword.isEmpty()) {
				Toast.makeText(activity,
						getResources().getString(R.string.string_provide_data),
						Toast.LENGTH_SHORT).show();
			} else {
				final LoginTask loginTask = new LoginTask(activity);
				loginTask.execute(textLogin, textPassword);
			}

		}
	};

}
