package de.kraflapps.skilltest.stylight;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * 
 * @author Veronika Rodionova
 * 
 */

public class InfoFragment extends ListFragment {

	public static final String KEYNAME_INFO = "Clothes_info";
	public static final String KEYNAME_DESCRIPTION = "name";
	public static final String KEYNAME_PRICE = "price";
	public static final String KEYNAME_SALE = "sale";
	public static final String KEYNAME_IMAGES = "images";
	public static final String KEYNAME_SAVINGS = "savings";
	public static final String KEYNAME_URL = "url";

	public FragmentActivity activity;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			final Bundle savedInstanceState) {

		activity = getActivity();
		JSONArray products = null;
		ArrayList<PairInfo> productList = null;

		final Bundle extras = getArguments();
		final String strJson = extras.getString(KEYNAME_INFO);
		if (strJson != null) {
			try {
				products = new JSONArray(strJson);
				productList = (ArrayList<PairInfo>) createProductList(products);
			} catch (JSONException e) {
				final String msg = activity.getResources().getString(
						R.string.string_unexpected_error);
				Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
				Log.d(LoginActivity.TAG, msg, e);
			}
		}

		if (productList != null) {
			final PairInfoAdapter adapter = new PairInfoAdapter(activity,
					productList);
			setListAdapter(adapter);
		}

		final View rootView = inflater.inflate(R.layout.fragment_info,
				container, false);

		return rootView;

	}

	private List<PairInfo> createProductList(JSONArray jsonArray)
			throws JSONException {

		final ArrayList<PairInfo> productList = new ArrayList<PairInfo>();

		final int jsonArrayLen = jsonArray.length();
		ItemInfo rightItemInfo;
		ItemInfo leftItemInfo;
		JSONObject rightObject;
		JSONObject leftObject;
		PairInfo product;
		for (int i = 0; i < jsonArrayLen; i = i + 2) {

			leftObject = jsonArray.getJSONObject(i);
			leftItemInfo = createItemInfo(leftObject);

			if (i + 1 < jsonArrayLen) {
				rightObject = jsonArray.getJSONObject(i + 1);
				rightItemInfo = createItemInfo(rightObject);
			} else {
				rightItemInfo = new ItemInfo(null, Double.NaN, Double.NaN,
						null, false);
			}

			product = new PairInfo(leftItemInfo, rightItemInfo);
			productList.add(product);
		}

		return productList;

	}

	private ItemInfo createItemInfo(JSONObject jsonObject) throws JSONException {
		ItemInfo itemInfo;

		final String description = jsonObject.getString(KEYNAME_DESCRIPTION);
		double price = jsonObject.getDouble(KEYNAME_PRICE);
		final JSONObject imageObject = jsonObject.getJSONArray(KEYNAME_IMAGES)
				.getJSONObject(0);
		final String imageLink = imageObject.getString(KEYNAME_URL);
		final boolean isOnSale = jsonObject.getBoolean(KEYNAME_SALE);
		double priceOld = Double.NaN;
		if (isOnSale) {
			final double savings = jsonObject.getDouble(KEYNAME_SAVINGS);
			priceOld = price;
			price = (1.0 - savings) * priceOld;
		}

		itemInfo = new ItemInfo(description, price, priceOld, imageLink,
				isOnSale);
		return itemInfo;
	}

}
