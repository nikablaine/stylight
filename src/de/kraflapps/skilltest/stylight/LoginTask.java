package de.kraflapps.skilltest.stylight;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

/**
 * 
 * @author Veronika Rodionova
 * 
 */

public class LoginTask extends AsyncTask<String, Void, HttpResponse> {

	public static final int SUCCESS_STATUS_CODE = 200;
	public static final String COOKIE_NAME = "Set-Cookie";
	public static final String COOKIE_ST_SECURE = "st_secure";

	public FragmentActivity activity;

	public LoginTask(FragmentActivity fragmentActivity) {
		super();
		activity = fragmentActivity;
	}

	@Override
	protected void onPreExecute() {
		final WaitingFragment waitFragment = new WaitingFragment();
		activity.getSupportFragmentManager().beginTransaction()
				.replace(R.id.container, waitFragment).commit();
	}

	@Override
	protected HttpResponse doInBackground(String... data) {

		final HttpClient httpClient = new DefaultHttpClient();
		final String parameters = '?' + LoginFragment.KEYNAME_LOGIN + '='
				+ data[0] + '&' + LoginFragment.KEYNAME_PASSWORD + '='
				+ data[1];
		final HttpPost httpPost = new HttpPost(LoginFragment.STYLIGHT_URL
				+ parameters);
		HttpResponse httpResponse = null;

		final List<NameValuePair> settings = new ArrayList<NameValuePair>();
		settings.add(new BasicNameValuePair(LoginFragment.KEYNAME_API_KEY,
				LoginFragment.X_API_KEY));

		try {
			httpPost.setEntity(new UrlEncodedFormEntity(settings));
		} catch (UnsupportedEncodingException e) {
			final String msg = activity.getResources().getString(
					R.string.string_auth_error_enc);
			Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
			Log.e(LoginActivity.TAG, msg, e);
		}

		try {
			httpResponse = httpClient.execute(httpPost);
		} catch (ClientProtocolException e) {
			final String msg = activity.getResources().getString(
					R.string.string_http_error);
			Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
			Log.e(LoginActivity.TAG, msg, e);
		} catch (IOException e) {
			final String msg = activity.getResources().getString(
					R.string.string_io_error);
			Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
			Log.e(LoginActivity.TAG, msg, e);
		}
		return httpResponse;
	}

	@Override
	protected void onPostExecute(HttpResponse result) {

		String cookie;
		final int statusCode = result.getStatusLine().getStatusCode();

		if (statusCode != SUCCESS_STATUS_CODE) {
			Toast.makeText(
					activity,
					activity.getResources().getString(
							R.string.string_login_error)
							+ ' ' + statusCode, Toast.LENGTH_SHORT).show();
			activity.getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new LoginFragment()).commit();
		} else {
			final Header[] headers = result.getHeaders(COOKIE_NAME);
			final HeaderElement[] elements = headers[1].getElements();
			final SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(activity);
			final SharedPreferences.Editor editor = prefs.edit();
			for (final HeaderElement element : elements) {
				if (element.getName().equals(COOKIE_ST_SECURE)) {
					cookie = element.getValue();
					editor.putString(COOKIE_ST_SECURE, cookie);
					editor.commit();
					(new GetInfoTask(activity)).execute();
				}
			}
		}
	}
}
